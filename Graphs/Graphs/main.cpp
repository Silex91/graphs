#pragma once
#include<iostream>
#include"CGraphe.h"
using namespace std;

CGraphe InverserArcs(CGraphe graParam)
{
	CGraphe G2;

	unsigned int uiNbSom = graParam.GetGRANbSommets();
	CSommet ** ppsomSom = graParam.GetGRASommets();

	for (int iBoucle1 = 0; iBoucle1 < uiNbSom; iBoucle1++)
	{
		unsigned int uiId = ppsomSom[iBoucle1]->GetSOMIdSommet();
		G2.GRAAjouterSommet(new CSommet(uiId));
	}
	for (int iBoucle1 = 0; iBoucle1 < uiNbSom; iBoucle1++) //POUR CHAQUE SOMMET
	{
		unsigned int uiId = ppsomSom[iBoucle1]->GetSOMIdSommet();
		int iNbarcPartant = ppsomSom[iBoucle1]->GetSOMNbArcsPartants();
		int iNbarcArrivant = ppsomSom[iBoucle1]->GetSOMNbArcsArrivants();

		CArc **arc = ppsomSom[iBoucle1]->GetSOMArcsPartants();
		int iPos = 0;

		for (int iBoucle2 = 0; iBoucle2 < iNbarcPartant; iBoucle2++) // CHAQUE ARC PARTANT
		{
			int iDest = arc[iBoucle2]->GetARCDestination();
			G2.GRAAjouterArcPartant(new CSommet(iDest), new CArc(uiId)); //On ajoute un arc dans le sommet ou l'ancien arc arrivait
			G2.GRAAjouterArcArrivant(ppsomSom[iBoucle1], new CArc(uiId));
		}
	}
	return G2;
}

int main(int argc, char* argv[])
{
	std::cout << "Projet Graphes " << std::endl << std::endl;
	if (argc > 1)
	{
		CGraphe * pGraphe = new CGraphe[argc - 1];
		CGraphe * pGrapheInv = new CGraphe[argc - 1];
		cout << "**************************************************" << endl;
		for (int iBoucle = 1; iBoucle < argc; iBoucle++)
		{
			pGraphe[iBoucle - 1].GRAParseurLecture(argv[iBoucle]);
			pGrapheInv[iBoucle - 1] = InverserArcs(pGraphe[iBoucle - 1]);
			cout << pGraphe[iBoucle - 1] << endl;
			cout << "--------------------------------------------------------------------" << endl;
			cout << pGrapheInv[iBoucle - 1] << endl;
		}
		cout << "**************************************************" << endl;
		system("PAUSE");
		return 0;
	}
	cout << "1\n";
	CGraphe G1;
	G1.GRAParseurLecture("test1.txt");
	cout << "2\n";
	cout << G1;
	cout << "3\n";

	CGraphe G2 = InverserArcs(G1);
	/*
	unsigned int uiNbSom = G1.GetGRANbSommets();
	CSommet ** ppsomSom = G1.GetGRASommets();

	for (int iBoucle1 = 0; iBoucle1 < uiNbSom; iBoucle1++)
	{
		unsigned int uiId = ppsomSom[iBoucle1]->GetSOMIdSommet();
		G2.GRAAjouterSommet(new CSommet(uiId));
	}
	for (int iBoucle1 = 0; iBoucle1 < uiNbSom; iBoucle1++) //POUR CHAQUE SOMMET
	{
		unsigned int uiId = ppsomSom[iBoucle1]->GetSOMIdSommet();
		int iNbarcPartant = ppsomSom[iBoucle1]->GetSOMNbArcsPartants();
		int iNbarcArrivant = ppsomSom[iBoucle1]->GetSOMNbArcsArrivants();

		CArc **arc = ppsomSom[iBoucle1]->GetSOMArcsPartants();
		int iPos = 0;

		for (int iBoucle2 = 0; iBoucle2 < iNbarcPartant; iBoucle2++) // CHAQUE ARC PARTANT
		{
			int iDest = arc[iBoucle2]->GetARCDestination();
			G2.GRAAjouterArcPartant(new CSommet(iDest), new CArc(uiId)); //On ajoute un arc dans le sommet ou l'ancien arc arrivait
			G2.GRAAjouterArcArrivant(ppsomSom[iBoucle1], new CArc(uiId));
		}
	}*/
	cout << G2 << endl << "4" << endl;
	G2.GRAParseurEcriture("test2.txt");

	CGraphe G3;
	G3.GRAAjouterSommet(new CSommet(3));
	G3.GRAAjouterSommet(new CSommet(5));
	G3.GRAAjouterSommet(new CSommet(6));
	G3.GRAAjouterArcPartant(new CSommet(3), new CArc(6));
	G3.GRAAjouterArcArrivant(new CSommet(6), new CArc(3));
	G3.GRAAjouterArcPartant(new CSommet(5), new CArc(3));
	G3.GRAAjouterArcArrivant(new CSommet(3), new CArc(5));
	
	cout << G3 << endl;
	system("PAUSE");
}