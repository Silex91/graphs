#include "CSommet.h"
#include"CException.h"


unsigned int CSommet::suiNbSommet = 0;

/* ----------------------------------------------------------
Nom : CSommet (Constructeur par defaut)
-------------------------------------------------------------
initialise un objet CSommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CSommet
---------------------------------------------------------- */
CSommet::CSommet()
{	
	++suiNbSommet;
	uiIdSommet = suiNbSommet;
	uiNbArcsPartants = 0;		
	uiNbArcsArrivants = 0;
	arcArcsPartants = NULL;		
	arcArcsArrivants = NULL;   
}

/* ----------------------------------------------------------
Nom : CSommet (Constructeur par parametres)
-------------------------------------------------------------
initialise un objet CSommet avec un identifiant
-------------------------------------------------------------
Entree(s)  :  Identifiant
Necessite  :  rien
Sortie(s)  :  CSommet
---------------------------------------------------------- */
CSommet::CSommet(unsigned int uiId)
{
	suiNbSommet += uiId;
	uiIdSommet = uiId;
	uiNbArcsPartants = 0;
	uiNbArcsArrivants = 0;
	arcArcsPartants = NULL;
	arcArcsArrivants = NULL;
}

/* ----------------------------------------------------------
Nom : CSommet (Constructeur par recopie)
-------------------------------------------------------------
initialise un objet CSommet similaire a un autre
-------------------------------------------------------------
Entree(s)  :  CSommet a recopier
Necessite  :  rien
Sortie(s)  :  CSommet
---------------------------------------------------------- */
CSommet::CSommet(const CSommet & somSommetParam)
{
	uiIdSommet = somSommetParam.uiIdSommet;
	uiNbArcsPartants = somSommetParam.uiNbArcsPartants;
	uiNbArcsArrivants = somSommetParam.uiNbArcsArrivants;

	arcArcsPartants = new CArc*[uiNbArcsPartants];
	for (unsigned int iBoucle = 0; iBoucle < uiNbArcsPartants; iBoucle++)
		arcArcsPartants[iBoucle] = new CArc(*somSommetParam.arcArcsPartants[iBoucle]);

	arcArcsArrivants = new CArc*[uiNbArcsArrivants];
	for (unsigned int iBoucle = 0; iBoucle < uiNbArcsArrivants; iBoucle++)
		arcArcsArrivants[iBoucle] = new CArc(*somSommetParam.arcArcsArrivants[iBoucle]);
}

/* ----------------------------------------------------------
Nom : CSommet (destructeur)
-------------------------------------------------------------
li�re la memoire allou�e par un objet CSommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
CSommet::~CSommet()
{
	if (arcArcsPartants)
	{
		for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsPartants; uiBoucle++)
			delete arcArcsPartants[uiBoucle];
		delete[] arcArcsPartants;
	}
	
	if (arcArcsArrivants)
	{
		for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsArrivants; uiBoucle++)
			delete arcArcsArrivants[uiBoucle];
		delete[] arcArcsArrivants;
	}
}

/* ----------------------------------------------------------
Nom : GetSOMNbSommet
-------------------------------------------------------------
Retourne la valeur de la variable statique du nombre de 
sommets crees
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
unsigned int CSommet::GetSOMNbSommet() { return suiNbSommet; }

/* ----------------------------------------------------------
Nom : SetSOMNbSommet
-------------------------------------------------------------
Modifie la variable statique du nombre de sommets crees
-------------------------------------------------------------
Entree(s)  :  unsigned int
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMNbSommet(static unsigned int suiNbSommetParam) { suiNbSommet = suiNbSommetParam; }

/* ----------------------------------------------------------
Nom : GetSOMIdSommet
-------------------------------------------------------------
Retourne l'id du sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  unsigned int
---------------------------------------------------------- */
unsigned int CSommet::GetSOMIdSommet() {return uiIdSommet;}

/* ----------------------------------------------------------
Nom : SetSOMIdSommet
-------------------------------------------------------------
Modifie l'id du sommet
-------------------------------------------------------------
Entree(s)  :  unsigned int
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMIdSommet(unsigned int uiIdSommetParam) {uiIdSommet = uiIdSommetParam;}

/* ----------------------------------------------------------
Nom : GetSOMNbArcsPartants
-------------------------------------------------------------
Retourne le nombre d'arcs partant du commey
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  unsigned int
---------------------------------------------------------- */
unsigned int CSommet::GetSOMNbArcsPartants() {return uiNbArcsPartants;}

/* ----------------------------------------------------------
Nom : SetSOMNbArcsPartants
-------------------------------------------------------------
Modifie le nombre d'arc partant du sommet
-------------------------------------------------------------
Entree(s)  :  unsigned int 
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMNbArcsPartants(unsigned int uiNbArcsPartantsParam) {uiNbArcsPartants = uiNbArcsPartantsParam;}

/* ----------------------------------------------------------
Nom : GetSOMNbArcsArrivants
-------------------------------------------------------------
Retourne le nombre d'arcs arrivant au sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  unsigned int 
---------------------------------------------------------- */
unsigned int CSommet::GetSOMNbArcsArrivants(){return uiNbArcsArrivants;}

/* ----------------------------------------------------------
Nom : SetSOMNbArcsArrivants
-------------------------------------------------------------
Modifie le nombre d'arc arrivant au sommet
-------------------------------------------------------------
Entree(s)  :  nouvelle valeur
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMNbArcsArrivants(unsigned int uiNbArcsArrivantsParam) {uiNbArcsArrivants = uiNbArcsArrivantsParam;}

/* ----------------------------------------------------------
Nom : GetSOMArcsPartants
-------------------------------------------------------------
Retourne le tableau d'arc partant du sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CArc **
---------------------------------------------------------- */
CArc ** CSommet::GetSOMArcsPartants() {return arcArcsPartants;}

/* ----------------------------------------------------------
Nom : SetSOMNbArcsPartants
-------------------------------------------------------------
Modifie le tableau d'arc partant du sommet
-------------------------------------------------------------
Entree(s)  :  CArc **
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMNbArcsPartants(CArc ** arcArcsPartantsParam) {arcArcsPartants = arcArcsPartantsParam;}

/* ----------------------------------------------------------
Nom : GetSOMArcsArrivants
-------------------------------------------------------------
Retourne le tableau d'arc partant du sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CArc **
---------------------------------------------------------- */
CArc ** CSommet::GetSOMArcsArrivants() {return arcArcsArrivants;}

/* ----------------------------------------------------------
Nom : SetSOMNbArcsArrivants
-------------------------------------------------------------
Modifie le tableau d'arc arrivant
-------------------------------------------------------------
Entree(s)  :  CArc **
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SetSOMNbArcsArrivants(CArc ** arcArcsArrivantsParam) {arcArcsArrivants = arcArcsArrivantsParam;}

/****************************************************************************/

/* ----------------------------------------------------------
Nom : SOMExistenceArcPartant
-------------------------------------------------------------
Verifie l'existence ou nom de l'arc passe en parametre
-------------------------------------------------------------
Entree(s)  :  CArc *
Necessite  :  rien
Sortie(s)  :  booleen
---------------------------------------------------------- */
bool CSommet::SOMExistenceArcPartant(CArc * arcParam)
{
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsPartants; uiBoucle++)
	{
		if (arcArcsPartants[uiBoucle]->GetARCDestination() == arcParam->GetARCDestination())
			return true;
	}
	return false;
}

/* ----------------------------------------------------------
Nom : SOMExistenceArcPartant
-------------------------------------------------------------
Verifie l'existence ou nom de l'arc passe en parametre
Recupere la position de l'arc dans trouve dans la variable
passee en parametre
-------------------------------------------------------------
Entree(s)  :  CArc *
			  int *
Necessite  :  rien
Sortie(s)  :  booleen
---------------------------------------------------------- */
bool CSommet::SOMExistenceArcPartant(CArc * arcParam, int * piPos)
{
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsPartants; uiBoucle++)
	{
		if (arcArcsPartants[uiBoucle]->GetARCDestination() == arcParam->GetARCDestination())
		{
			*piPos = uiBoucle;
			return true;
		}
	}
	*piPos = -1;
	return false;
}

/* ----------------------------------------------------------
Nom : SOMRecopie
-------------------------------------------------------------
Cette fonction permet de recopier les (n-1) adresses des Arcs
dans un nouveau tableau de taille n
On utilise cette fonction en vue d'agrandir le tableau d'arc
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CArc **
---------------------------------------------------------- */
CArc ** CSommet::SOMRecopie()
{
	CArc ** pparcNewArc = new CArc*[uiNbArcsPartants];
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsPartants - 1; uiBoucle++)
	{
		pparcNewArc[uiBoucle] = new CArc(*arcArcsPartants[uiBoucle]);
	}
	return pparcNewArc;
}

/* ----------------------------------------------------------
Nom : SOMRecopiePartielle
-------------------------------------------------------------
Cette fonction permet de recopier les adresses du tableau 
d'arc a l'exception d'une dont la position est passee en 
parametre. 
On utilise cette fonction en vue de supprimer le tableau d'arc
-------------------------------------------------------------
Entree(s)  :  unsigned int
Necessite  :  rien
Sortie(s)  :  CArc **
---------------------------------------------------------- */
CArc ** CSommet::SOMRecopiePartielle(unsigned int uiIndice)
{
	CArc ** pparcNewArc = new CArc*[uiNbArcsPartants];
	for (unsigned int uiBoucle = 0; uiBoucle < uiIndice; uiBoucle++)
	{
		pparcNewArc[uiBoucle] = new CArc(*arcArcsPartants[uiBoucle]);
	}
	for (unsigned int uiBoucle = uiIndice + 1; uiBoucle < uiNbArcsPartants; uiBoucle++)
	{
		pparcNewArc[uiBoucle - 1] = new CArc(*arcArcsPartants[uiBoucle]);
	}
	return pparcNewArc;
}

/* ----------------------------------------------------------
Nom : SOMAjouterArcPartant
-------------------------------------------------------------
Cette fonction permet d'ajouter un arc a la derniere position
du tableau d'arcs partant du sommet.
-------------------------------------------------------------
Entree(s)  :  CArc *
Necessite  :  rien
Sortie(s)  :  booleen
---------------------------------------------------------- */
void CSommet::SOMAjouterArcPartant(CArc * arcArcParam)
{
	try {
		if (arcArcParam->GetARCDestination() == uiIdSommet)
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Impossible d'ajouter : l'arc boucle sur un seul sommet ! ");
			return;
	}
	try{
		if (SOMExistenceArcPartant(arcArcParam))
		{
			throw Cexception();
			return;
		}
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("L'arc existe deja ! ");
	}
	uiNbArcsPartants++;
	CArc ** pparcTemp = SOMRecopie();
	delete arcArcsPartants;
	arcArcsPartants = pparcTemp;
	arcArcsPartants[uiNbArcsPartants - 1] = arcArcParam;
}

/* ----------------------------------------------------------
Nom : SOMSuprrimerArcPartant
-------------------------------------------------------------
Cette fonction supprime l'arc passe en parametre dans le 
tableau d'arc partant
-------------------------------------------------------------
Entree(s)  :  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SOMSuprrimerArcPartant(CArc * arcArcParam)
{
	int iPos;
	try {
		if (!SOMExistenceArcPartant(arcArcParam, &iPos))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Impossible de Supprimer : l'arc n'existe pas ! ");
		return;
	}
	
	CArc ** pparcTemp = SOMRecopiePartielle(iPos);
	delete[] arcArcsPartants;
	arcArcsPartants = pparcTemp;
	uiNbArcsPartants--;
}

/* ----------------------------------------------------------
Nom : SOMModifierArcPartant
-------------------------------------------------------------
Cette fonction permet de remplacer un arc par un autre.
-------------------------------------------------------------
Entree(s)  :  CArc *
			  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CSommet::SOMModifierArcPartant(CArc * arcArcAvant, CArc * arcArcApres)
{
	int iPos;
	try {
		if (!SOMExistenceArcPartant(arcArcAvant, &iPos))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Impossible de modifier : l'arc n'existe pas dans le tableau ! ");
		return;
	}
	delete arcArcsPartants[iPos];
	arcArcsPartants[iPos] = arcArcApres;
}

/************************************************************************/
/*
bool CSommet::SOMExistenceArcArrivant(CArc * arcParam)
{
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsArrivants; uiBoucle++)
	{
		if (arcArcsArrivants[uiBoucle] == arcParam)
			return true;
	}
	return false;
}

bool CSommet::SOMExistenceArcArrivant(CArc * arcParam, int * piPos)
{
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsPartants; uiBoucle++)
	{
		if (arcArcsArrivants[uiBoucle] == arcParam)
		{
			*piPos = uiBoucle;
			return true;
		}
	}
	*piPos = -1;
	return false;
}*/

CArc ** CSommet::SOMRecopieArrivant()
{
	CArc ** pparcNewArc = new CArc*[uiNbArcsArrivants];
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcsArrivants - 1; uiBoucle++)
	{
		pparcNewArc[uiBoucle] = new CArc(*arcArcsArrivants[uiBoucle]);
	}
	return pparcNewArc;
}

CArc ** CSommet::SOMRecopiePartielleArrivant(unsigned int uiIndice)
{
	CArc ** pparcNewArc = new CArc*[uiNbArcsArrivants];
	for (unsigned int uiBoucle = 0; uiBoucle < uiIndice; uiBoucle++)
	{
		pparcNewArc[uiBoucle] = new CArc(*arcArcsArrivants[uiBoucle]);
	}
	for (unsigned int uiBoucle = uiIndice + 1; uiBoucle < uiNbArcsArrivants; uiBoucle++)
	{
		pparcNewArc[uiBoucle - 1] = new CArc(*arcArcsArrivants[uiBoucle]);
	}
	return pparcNewArc;
}

void CSommet::SOMAjouterArcArrivant(CArc * arcArcParam)
{
	/*try {
		if (SOMExistenceArcArrivant(arcArcParam))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur AJouterArcArrivant : l'arc existe deja ! ");
		return;
	}*/
	uiNbArcsArrivants++;
	CArc ** pparcTemp = SOMRecopieArrivant();
	delete[] arcArcsArrivants;
	arcArcsArrivants = pparcTemp;
	arcArcsArrivants[uiNbArcsArrivants - 1] = arcArcParam;
}

void CSommet::SOMSuprrimerArcArrivant(CArc * arcArcParam)
{
	int iPos = 0;
	/*try {
		if (!SOMExistenceArcArrivant(arcArcParam, &iPos))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur SupprimerArcArrivant : l'arc n'existe pas ! ");
		return;
	}*/
	CArc ** pparcTemp = SOMRecopiePartielleArrivant(iPos);
	delete[] arcArcsArrivants;
	arcArcsArrivants = pparcTemp;
	uiNbArcsArrivants--;
}

void CSommet::SOMModifierArcArrivant(CArc * arcArcAvant, CArc * arcArcApres)
{
	int iPos = 0;
	/*try {
		if (!SOMExistenceArcArrivant(arcArcAvant, &iPos))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur ModifierArrivant : l'arc n'existe pas ! ");
		return;
	}*/
	delete arcArcsArrivants[iPos];
	arcArcsArrivants[iPos] = arcArcApres;
}
