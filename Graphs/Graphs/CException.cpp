
/*CLASSE Cexception
  DOCUMENTATION
	Attributs : uiEXCvaleur, entier, contient la valeur de l'exception
	Structure : Cette classe contient une m�thode de modification et une m�thode
			de consultation de la valeur de l'exception
	M�thode : n�ant
	Modules internes :*/
#include<iostream>
#include <fstream>
#include "CException.h"


//CORPS

/**************************************************************
Nom : Cexception
***************************************************************
Constructeur par d�faut de la classe Cexception : permet
d'initialiser un objet
***************************************************************
Entr�e : rien
N�cessite : n�ant
Sortie : rien
Entra�ne : L'exception est intialis�e ?FAUX
***************************************************************/
Cexception::Cexception()
{
	uiEXCvaleur = FAUX;
	//l'exception est initialis?
}

/**************************************************************
Nom : EXCmodifier_valeur
***************************************************************
Cette fonction permet de modifier la valeur de l'exception
***************************************************************
Entr�e : la nouvelle valeur de l'exception
N�cessite : n�ant
Sortie : rien
Entra�ne : L'exception est modifi�e
***************************************************************/
void Cexception::EXCmodifier_valeur(unsigned int val)
{
	uiEXCvaleur = val;
	//l'exception est modifi�e
}

/**************************************************************
Nom : EXCAfficherException
***************************************************************
Cette fonction permet d'afficher l'exception ?l'�cran
***************************************************************
Entr�e : Le message ?afficher
N�cessite : n�ant
Sortie : rien
Entra�ne : L'exception est Affich�e
***************************************************************/
void Cexception::EXCAfficherException(const char* ccMessage)
{
	std::cout << "----------------------------------------------------\n";
	std::cout << "Une exception a ete levee avec le code : " << EXClire_valeur() << std::endl << ccMessage << std::endl;
	std::cout << "----------------------------------------------------\n";
}
