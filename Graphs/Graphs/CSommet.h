#pragma once
/*-------------------------------------------------------------*
| Titre : Classe pour repr��senterer des sommet dans le graphe  |
| ------------------------------------------------------------ |
| Auteur    : A. El-fahim / A. Cassagnaud                      |
| Version   : 1.0                                              |
| Date	    :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date	    :                                                  |
| ------------------------------------------------------------ |
| Auteur    :                                                  |
| Version	:                                                  |
| Date	    :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date	    :                                                  |
| ------------------------------------------------------------ |
| Interface de la classe CSommet.                              |
| Cette classe permet de ceer des objets SOMMET.               |
*-------------------------------------------------------------*/

#include<iostream>
#include<stdio.h>
#include"CArc.h"
using namespace std;

/*------------------------------------------------------------*/
/*! \class CSomment
  * \brief classe qui permet repr��senterer des sommet dans le graphe
  */
class CSommet
{
private:
	static unsigned int suiNbSommet;   /*!< Nombre de sommets existant */
	unsigned int uiIdSommet;            /*!< Numero du sommet */
	unsigned int uiNbArcsPartants;		/*!< Nombre d'Arcs entrants  */
	unsigned int uiNbArcsArrivants;		/*!< Nombre d'Arcs partants  */
	CArc ** arcArcsPartants;		    /*!< Arcs partants du sommet */
	CArc ** arcArcsArrivants;           /*!< Arcs arrivants au sommet */
public:
	/*------------ Constructeur par defaut -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CSommet();
	/*------------ Constructeur avec parametres -------------
	Entree(s)  :  Id du sommet
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CSommet(unsigned int);
	/*------------ Constructeur par recopie -------------
	Entree(s)  :  CSommet
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CSommet(const CSommet&);
	/*------------ destructeur -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	~CSommet();

	/*------------ Methode Get -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	static unsigned int GetSOMNbSommet();
	/*------------ Methode Set -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMNbSommet(unsigned int suiNbSommetParam);

	/*------------ Methode Get -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	unsigned int GetSOMIdSommet();
	/*------------ Methode Set -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMIdSommet(unsigned int uiIdSommetParam);

	/*------------ Methode Get -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	unsigned int GetSOMNbArcsPartants();
	/*------------ Methode Set -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMNbArcsPartants(unsigned int uiNbArcsPartantsParam);

	/*------------ Methode Get -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	unsigned int GetSOMNbArcsArrivants();
	/*------------ Methode Set -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMNbArcsArrivants(unsigned int uiNbArcsArrivantsParam);

	/*------------ Methode Get -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CArc ** GetSOMArcsPartants();
	/*------------ Methode Set -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMNbArcsPartants(CArc ** arcArcsPartantsParam);

	/*------------ Methode Get -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CArc ** GetSOMArcsArrivants();
	/*------------ Methode Set -------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetSOMNbArcsArrivants(CArc ** arcArcsArrivantsParam);
/*-------------------------------------------------------------------------*/
/*----------ArcsPartants---------------------------------------------------*/
/*-------------------------------------------------------------------------*/

	/*------------ Methode ---------------------------------
	Entree(s)  :  rien
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	bool SOMExistenceArcPartant(CArc*);
	bool SOMExistenceArcPartant(CArc*, int*);
	CArc ** SOMRecopie();
	CArc ** SOMRecopiePartielle(unsigned int);
	void SOMAjouterArcPartant(CArc * arcArcParam);
	void SOMSuprrimerArcPartant(CArc * arcArcParam);
	void SOMModifierArcPartant(CArc * arcArcAvant, CArc * arcArcApres);
/*------------ArcsArrivants---------------------------------------------------*/
	//bool SOMExistenceArcArrivant(CArc*);
	//bool SOMExistenceArcArrivant(CArc*, int*);
	CArc ** SOMRecopieArrivant();
	CArc ** SOMRecopiePartielleArrivant(unsigned int);
	void SOMAjouterArcArrivant(CArc * arcArcParam);
	void SOMSuprrimerArcArrivant(CArc * arcArcParam);
	void SOMModifierArcArrivant(CArc * arcArcAvant, CArc * arcArcApres);

};