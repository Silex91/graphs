#include "CArc.h"

/* ----------------------------------------------------------
Nom : CArc (Constructeur avec parametre)
-------------------------------------------------------------
Initialise un objet CArc avec sa destination
-------------------------------------------------------------
Entree(s)  :  Destination de l'arc
Necessite  :  rien
Sortie(s)  :  objet arc
---------------------------------------------------------- */
CArc::CArc(unsigned int uiDestinationParam)
{
	SetARCDestination(uiDestinationParam);
}

/* ----------------------------------------------------------
Nom : GetARCDestination
-------------------------------------------------------------
Retourne la desination de l'arc
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  destination de l'arc
---------------------------------------------------------- */
unsigned int CArc::GetARCDestination()
{
	return uiDestination;
}

/* ----------------------------------------------------------
Nom : SetARCDestination
-------------------------------------------------------------
Modifie la desination de l'arc
-------------------------------------------------------------
Entree(s)  :  destination de l'arc
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CArc::SetARCDestination(unsigned int uiDestinationParam)
{
	uiDestination = uiDestinationParam;
}



