#pragma once
/*-------------------------------------------------------------*
| Titre : Classe pour repr��senterer des arcs dans le graphe    |
| ------------------------------------------------------------ |
| Auteur    : A. El-fahim / A. Cassagnaud                      |
| Version   : 1.0                                              |
| Date	    :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date	    :                                                  |
| ------------------------------------------------------------ |
| Auteur    :                                                  |
| Version	:                                                  |
| Date	    :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date	    :                                                  |
| ------------------------------------------------------------ |
| Interface de la classe CArc.                                 |
| Cette classe permet de ceer des objets ARC.                  |
*-------------------------------------------------------------*/

#include<iostream>
#include<stdio.h>
using namespace std;

/*------------------------------------------------------------*/
/*! \class CArc
  * \brief classe qui permet repr��senterer des arcs dans le graphe
  */
class CArc
{
private:
	unsigned int uiDestination;	/*!< Num��ro du sommet destination */
public:
	/*------------ Constructeur avec parametres -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	CArc(unsigned int uiDestinationParam);

	/*------------ Methode Get -------------
	Entree(s)  :  
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	unsigned int GetARCDestination();

	/*------------ Methodes Set -------------
	Entree(s)  :  destination de l'arc
	Necessite  :  rien
	Sortie(s)  :  rien
	-------------------------------------------------------*/
	void SetARCDestination(unsigned int uiDestinationParam);

	

};

