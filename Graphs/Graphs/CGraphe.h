#pragma once
/*-------------------------------------------------------------*
| Titre : Classe pour représenterer des graphes                |
| ------------------------------------------------------------ |
| Auteur    : A. El-fahim / A. Cassagnaud                      |
| Version   : 1.0                                              |
| Date        :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date        :                                                  |
| ------------------------------------------------------------ |
| Auteur    :                                                  |
| Version    :                                                  |
| Date        :                                                  |
| -------------------------                                    |
| Lecteur   :                                                  |
| Date        :                                                  |
| ------------------------------------------------------------ |
| Interface de la classe CGraphe.                              |
| Cette classe permet de ceer des objets GRAPHE.               |
*-------------------------------------------------------------*/

#include<iostream>
#include<stdio.h>
#include"CSommet.h"
using namespace std;

/*------------------------------------------------------------*/
/*! \class CGraphe
  * \brief classe qui permet représenterer des graphe
  */
class CGraphe
{
private:
	unsigned int uiGRANbSommets;	/*!< Entier qui stocke le nombre de sommets presents dans le tableau */
	unsigned int uiGRANbArcs;		/*!< Entier qui stocke le nombre d'arc presents dans le tableau */
	CSommet ** ppsomGRASommets;    /*!< Tableau de Csommet qui seront les sommets du graphe */
public:
/*-------------------------------------------------*/
/*--------------Constructeurs----------------------*/
/*-------------------------------------------------*/
	CGraphe();
	CGraphe(const CGraphe&);
	~CGraphe();

/*-------------------------------------------------*/
/*--------------Methodes---------------------------*/
/*-------------------------------------------------*/
	unsigned int GetGRANbSommets() { return uiGRANbSommets; }
	unsigned int GetGRANbArcs() { return uiGRANbArcs; }
	CSommet ** GetGRASommets() { return ppsomGRASommets; }
	CSommet ** GRARecopie();
	CSommet ** GRARecopiePartielle(unsigned int);

	bool GRAExistenceSommet(CSommet *);
	bool GRAExistenceSommet(CSommet*, int*);

/**************************************************************/
	void GRAAjouterSommet(CSommet *);
	void GRASupprimerSommet(CSommet *);

	void GRAAjouterArcPartant(CSommet *, CArc *);
	void GRASuprimerArcPartant(CSommet *, CArc *);

	void GRAAjouterArcArrivant(CSommet *, CArc *);
	void GRASuprimerArcArrivant(CSommet *, CArc *);
/***************************************************************/
	char* GRAConcatenation(const char*, const char*);
	char * GRATraiterMot(char*);

	void GRAParseurEcriture(const char*);
	void GRAParseurLecture(const char*);
/***************************************************************/
	friend ostream& operator<<(ostream& os, const CGraphe & graGrapheParam);
};