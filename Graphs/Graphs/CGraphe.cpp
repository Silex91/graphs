#include "CGraphe.h"
#include "CException.h"
#include<iostream>
#include<fstream>
#include<string>
#include<windows.h>
#include <direct.h>
#define GetCurrentDir _getcwd


/*-------------------------------------------------*/
/*--------------Constructeurs----------------------*/
/*-------------------------------------------------*/

/* ----------------------------------------------------------
Nom : CGraphe (Constructeur par defaut)
-------------------------------------------------------------
initialise un objet CGraphe
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CGraphe
---------------------------------------------------------- */
CGraphe::CGraphe() :
	ppsomGRASommets(NULL), uiGRANbSommets(0)
{}

/* ----------------------------------------------------------
Nom : CGraphe (Constructeur par recopie)
-------------------------------------------------------------
initialise un objet CGraphe identique a celui en parametre
-------------------------------------------------------------
Entree(s)  :  CGraphe
Necessite  :  rien
Sortie(s)  :  CGraphe
---------------------------------------------------------- */
CGraphe::CGraphe(const CGraphe& graParam)
{
	uiGRANbArcs = graParam.uiGRANbArcs;
	uiGRANbSommets = graParam.uiGRANbSommets;
	ppsomGRASommets = new CSommet*[uiGRANbSommets];
	for (unsigned int uiBoucle = 0; uiBoucle < uiGRANbSommets; uiBoucle++)
	{
		if (graParam.ppsomGRASommets[uiBoucle])
			ppsomGRASommets[uiBoucle] = new CSommet(*(graParam.ppsomGRASommets[uiBoucle]));
		else
			ppsomGRASommets[uiBoucle] = NULL;
	}
}

/* ----------------------------------------------------------
Nom : CGraphe (Destructeur)
-------------------------------------------------------------
Libere la memoire allouee par un objet CGraphe
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
CGraphe::~CGraphe()
{
	if (ppsomGRASommets)
	{
		for (unsigned int uiBoucle = 0; uiBoucle < uiGRANbSommets; uiBoucle++)
			delete ppsomGRASommets[uiBoucle];
		delete[] ppsomGRASommets;
	}
}


/*-------------------------------------------------*/
/*----------------Methodes-------------------------*/
/*-------------------------------------------------*/

/* ----------------------------------------------------------
Nom : GRARecopie 
-------------------------------------------------------------
Recopie le tableau de sommets
On utilise cette fonction en vue d'ajouter un sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CSommet **
---------------------------------------------------------- */
CSommet ** CGraphe::GRARecopie()
{
	CSommet ** ppsomNewSommet = new CSommet*[uiGRANbSommets];
	for (unsigned int uiBoucle = 0; uiBoucle < uiGRANbSommets - 1; uiBoucle++)
	{
		ppsomNewSommet[uiBoucle] = ppsomGRASommets[uiBoucle];
	}
	return ppsomNewSommet;
}

/* ----------------------------------------------------------
Nom : GRARecopiePartielle
-------------------------------------------------------------
Recopie le tableau de sommets sauf celui a la position 
passee en parametre
On utilise cette fonction en vue de supprimer un sommet
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  CSommet **
---------------------------------------------------------- */
CSommet ** CGraphe::GRARecopiePartielle(unsigned int uiIndice)
{
	CSommet ** ppsomNewSommet = new CSommet*[uiGRANbSommets - 1];
	for (unsigned int uiBoucle = 0; uiBoucle < uiIndice; uiBoucle++)
	{
		ppsomNewSommet[uiBoucle] = ppsomGRASommets[uiBoucle];
	}
	for (unsigned int uiBoucle = uiIndice + 1; uiBoucle < uiGRANbSommets; uiBoucle++)
	{
		ppsomNewSommet[uiBoucle - 1] = ppsomGRASommets[uiBoucle];
	}
	return ppsomNewSommet;
}


/* ----------------------------------------------------------
Nom : GRAExistenceSommet
-------------------------------------------------------------
Indique l'existence ou non du sommet passe en parametre 
Si c'est le cas, sa position est stocke dans la variable en
parametre
-------------------------------------------------------------
Entree(s)  :  CSommet *
Necessite  :  rien
Sortie(s)  :  booleen
---------------------------------------------------------- */
bool CGraphe::GRAExistenceSommet(CSommet * somParam)
{
	try {
		if (!somParam)
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur : le sommet en parametre est nul ! ");
		return false;
	}
	for (unsigned int uiBoucle = 0; uiBoucle < uiGRANbSommets; uiBoucle++)
	{
		if (ppsomGRASommets[uiBoucle]->GetSOMIdSommet() == somParam->GetSOMIdSommet())
			return true;
	}
	return false;
}

bool CGraphe::GRAExistenceSommet(CSommet * somParam, int * piPos)
{
	try {
		if (!somParam)
		{
			throw Cexception();
			*piPos = -1;
		}
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur : le sommet en parametre est nul ! ");
		return false;
	}
	for (unsigned int uiBoucle = 0; uiBoucle < uiGRANbSommets; uiBoucle++)
	{
		if (ppsomGRASommets[uiBoucle]->GetSOMIdSommet() == somParam->GetSOMIdSommet())
		{
			*piPos = uiBoucle;
			return true;
		}
	}
	*piPos = -1;
	return false;
}


/* ----------------------------------------------------------
Nom : GRAAjouterSommet
-------------------------------------------------------------
Ajoute un sommet a la fin du tableau
-------------------------------------------------------------
Entree(s)  :  rien
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRAAjouterSommet(CSommet * somParam)
{
	if (GRAExistenceSommet(somParam))
	{
		return;
	}
	uiGRANbSommets++;
	CSommet ** ppsomTemp = GRARecopie();
	delete[] ppsomGRASommets;
	ppsomGRASommets = ppsomTemp;
	ppsomGRASommets[uiGRANbSommets - 1] = somParam;
}

void CGraphe::GRASupprimerSommet(CSommet * somParam)
{
	int iPos;
	if (!GRAExistenceSommet(somParam, &iPos))
	{

	}
	CSommet ** ppsomTemp = GRARecopiePartielle(iPos);
	delete[] ppsomGRASommets;
	ppsomGRASommets = ppsomTemp;
	uiGRANbSommets--;
}


/* ----------------------------------------------------------
Nom : GRAAjouterArcPartant
-------------------------------------------------------------
Ajoute un arc partant d'un des sommets du grapge
-------------------------------------------------------------
Entree(s)  :  CSommet *
			  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRAAjouterArcPartant(CSommet * psomParam, CArc *parcParam)
{
	int iPos;
	try {
		if (!GRAExistenceSommet(psomParam, &iPos))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur : le sommet n'existe pas ! Impossible d'ajouter l'arc ! ");
		return;
	}
	ppsomGRASommets[iPos]->SOMAjouterArcPartant(parcParam);
}

/* ----------------------------------------------------------
Nom : GRASuprimerArcPartant
-------------------------------------------------------------
Supprime un arc partant d'un des sommets du grapge
-------------------------------------------------------------
Entree(s)  :  CSommet *
			  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRASuprimerArcPartant(CSommet *psomParam, CArc *parcParam)
{
	try {
		if (!GRAExistenceSommet(psomParam))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur : le sommet n'existe pas ! Impossible de supprimer l'arc ! ");
		return;
	}
	psomParam->SOMSuprrimerArcPartant(parcParam);
}

/* ----------------------------------------------------------
Nom : GRAAjouterArcArrivant
-------------------------------------------------------------
Ajoute un arc Arrivant d'un des sommets du grapge
-------------------------------------------------------------
Entree(s)  :  CSommet *
			  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRAAjouterArcArrivant(CSommet * psomParam, CArc *parcParam)
{
	try {
		if (!GRAExistenceSommet(psomParam))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur : Le sommet n'existe pas ! ");
		return;
	}
	uiGRANbArcs++;
	psomParam->SOMAjouterArcArrivant(parcParam);
}

/* ----------------------------------------------------------
Nom : GRASuprimerArcArrivant
-------------------------------------------------------------
Supprime un arc Arrivant d'un des sommets du grapge
-------------------------------------------------------------
Entree(s)  :  CSommet *
			  CArc *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRASuprimerArcArrivant(CSommet *psomParam, CArc *parcParam)
{
	try {
		if (!GRAExistenceSommet(psomParam))
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Le sommet n'existe pas ! ");
		return;
	}
	uiGRANbArcs--;
	psomParam->SOMSuprrimerArcArrivant(parcParam);
}


/* ----------------------------------------------------------
Nom : GRAConcatenation
-------------------------------------------------------------
Concatenne deux chaines de caracteres
-------------------------------------------------------------
Entree(s)  :  const char *
			  const char *
Necessite  :  rien
Sortie(s)  :  char *
---------------------------------------------------------- */
char * CGraphe::GRAConcatenation(const char* pcNomFichierParam, const char* pcCheminParam)
{
	/*------------- Declaration variables --------------*/
	char* pcCheminFinal;
	unsigned int uiBoucleNomFichier = 0;
	unsigned int uiBoucleChemin = 0;

	/*--------- Calcul longueur des parametres ---------*/
	unsigned int uiLongueurNomFichier = 0;
	while (pcNomFichierParam[uiLongueurNomFichier] != '\0')
		uiLongueurNomFichier++;
	unsigned int uiLongueurChemin = 0;
	while (pcCheminParam[uiLongueurChemin] != '\0')
		uiLongueurChemin++;

	/*------------------ Concatenation ------------------*/
	unsigned int  uiLongueur = uiLongueurNomFichier + 2 + uiLongueurChemin + 1;
	pcCheminFinal = new char[uiLongueur];

	while (pcNomFichierParam[uiBoucleNomFichier] != '\0')
	{
		pcCheminFinal[uiBoucleNomFichier] = pcNomFichierParam[uiBoucleNomFichier];
		uiBoucleNomFichier++;
	}
	while (pcCheminParam[uiBoucleChemin] != '\0')
	{
		pcCheminFinal[uiBoucleNomFichier] = pcCheminParam[uiBoucleChemin];
		uiBoucleNomFichier++; uiBoucleChemin++;
	}
	pcCheminFinal[uiBoucleNomFichier] = '\0';
	/*--------------------- Sortie --------------------*/
	return pcCheminFinal;
}

/* ----------------------------------------------------------
Nom : GRATraiterMot
-------------------------------------------------------------
Cette fonction permet la lecture d'un fichier graphe. Elle 
separe les mots a partir du symbole '='
-------------------------------------------------------------
Entree(s)  :  char *
Necessite  :  rien
Sortie(s)  :  char *
---------------------------------------------------------- */
char * CGraphe::GRATraiterMot(char * pcflux)
{
	char * pcValeur = new char[20];
	unsigned int uiBoucleTemp = 0; unsigned int uiBoucleValeur = 0;

	while (pcflux[uiBoucleTemp] != '=')
	{
		try {
			if (pcflux[uiBoucleTemp] == '\0')
				throw Cexception();
		}
		catch (Cexception exc)
		{
			exc.EXCAfficherException("Erreur lors du traitement du mot � lire ! ");
			return NULL;
		}
		uiBoucleTemp++;
	} uiBoucleTemp++;
	try {
		if (pcflux[uiBoucleTemp] == '\0')
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur lors du traitement du mot � lire ! ");
		return NULL;
	}
	while (pcflux[uiBoucleTemp] != '\0')
	{
		if (pcflux[uiBoucleTemp] == ',')
		{
			pcValeur[uiBoucleValeur] = '\0';
			return pcValeur;
		}
		pcValeur[uiBoucleValeur] = pcflux[uiBoucleTemp];
		uiBoucleTemp++; uiBoucleValeur++;
	}
	pcValeur[uiBoucleValeur] = pcflux[uiBoucleTemp];

	return pcValeur;
}

/* ----------------------------------------------------------
Nom : GRAParseurEcriture
-------------------------------------------------------------
Cette fonction permet d'ecrire toute les informations du graphe
dans un fichier texte passe en parametre
-------------------------------------------------------------
Entree(s)  :  const char *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRAParseurEcriture(const char* pcFichier)
{
/*------R�cup�ration du chemin complet pour ouvrir le fichier--------------------*/
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	std::string current_working_dir(buff);
	string s = current_working_dir += "\\";
	char* sCurrentDir = (char*)s.c_str();
	char * pcCheminComplet = GRAConcatenation(sCurrentDir, pcFichier);
/*---------------------------------------------------------------------------------*/
/*------Ouverture du fichier-------------------------------------------------------*/
	ofstream ofsFlux;
	ofsFlux.open(pcCheminComplet, ios::out);
	try {
		if (!ofsFlux)
			throw Cexception();
	}
	catch (Cexception exc)
	{
		exc.EXCAfficherException("Erreur lors de l'ouverture du fichier o� �crire ! ");
		return;
	}
/*---------------------------------------------------------------------------------*/
/*------Ecriture dans le fichier---------------------------------------------------*/
	ofsFlux << "NBSommets=" << uiGRANbSommets << endl;
	ofsFlux << "NBArcs=" << uiGRANbArcs << endl;
	ofsFlux << "Sommets[" << endl;
	for (unsigned int uiBoucleSommet = 0; uiBoucleSommet < uiGRANbSommets; uiBoucleSommet++)
	{
		ofsFlux << "NumeroSommet=" << (*ppsomGRASommets[uiBoucleSommet]).GetSOMIdSommet() << endl;
	}
	ofsFlux << "]" << endl << "Arcs=[" << endl;
	for (unsigned int uiBoucleSommet = 0; uiBoucleSommet < uiGRANbArcs; uiBoucleSommet++)
	{
		unsigned int NbArc = (*ppsomGRASommets[uiBoucleSommet]).GetSOMNbArcsPartants();
		for (unsigned int uiBoucleArc = 0; uiBoucleArc < NbArc; uiBoucleArc++)
		{
			ofsFlux << "Debut=" << (*ppsomGRASommets[uiBoucleSommet]).GetSOMIdSommet() << ", Fin=";
			CArc ** arcTemp = (*ppsomGRASommets[uiBoucleSommet]).GetSOMArcsPartants();
			ofsFlux << (*arcTemp[uiBoucleArc]).GetARCDestination() << endl;
		}
	}
	ofsFlux << "]" << endl;

	ofsFlux.close();

}

/* ----------------------------------------------------------
Nom : GRAParseurLecture
-------------------------------------------------------------
Cette fonction permet de lire toute les informations dans un 
fichier texte passe en parametre afin de completer le graphe
-------------------------------------------------------------
Entree(s)  :  Const *
Necessite  :  rien
Sortie(s)  :  rien
---------------------------------------------------------- */
void CGraphe::GRAParseurLecture(const char* pcFichier)
{
/*------R�cup�ration du chemin complet pour ouvrir le fichier--------------------*/
	char buff[FILENAME_MAX];
	GetCurrentDir(buff, FILENAME_MAX);
	std::string current_working_dir(buff);
	string s = current_working_dir += "\\";
	char* sCurrentDir = (char*)s.c_str();
	char * pcCheminComplet = GRAConcatenation(sCurrentDir, pcFichier);
	
/*---------------------------------------------------------------------------------*/
/*------Ouverture du fichier-------------------------------------------------------*/
	ifstream ifsFlux;
	ifsFlux.open(pcCheminComplet, ios::in);
	try {
		if (!ifsFlux)
			throw Cexception();
	}catch(Cexception exc)
	{
		exc.EXCAfficherException("Erreur lors de l'ouverture du fichier a lire ! ");
		return;
	}
/*---------------------------------------------------------------------------------*/
/*------Lecture du fichier---------------------------------------------------------*/
	unsigned int uiNbSommets;
	unsigned int uiNbArcs;
	char * pcFlux = new char[40];
	ifsFlux >> pcFlux; //NBSommet
	char * pcValeur = GRATraiterMot(pcFlux);
	uiNbSommets = atoi(pcValeur);
	delete[] pcValeur;

	ifsFlux >> pcFlux; //NBArcs
	pcValeur = GRATraiterMot(pcFlux);
	uiNbArcs = atoi(pcValeur);
	delete[] pcValeur;

	ifsFlux >> pcFlux; //Sommet[
	for (unsigned int uiBoucle = 0; uiBoucle < uiNbSommets; uiBoucle++)
	{
		ifsFlux >> pcFlux; //NUMERO
		pcValeur = GRATraiterMot(pcFlux);
		unsigned int uitemp = atoi(pcValeur);
		CSommet * somTemp = new CSommet(uitemp);
		try {
			if (GRAExistenceSommet(somTemp))
				throw Cexception();
		}
		catch (Cexception exc)
		{
			exc.EXCAfficherException("Erreur lecture : le sommet existe deja ");
		}
		GRAAjouterSommet(somTemp);
		delete[] pcValeur;
	}
	ifsFlux >> pcFlux; //]
	ifsFlux >> pcFlux; //Arcs=[

	for (unsigned int uiBoucle = 0; uiBoucle < uiNbArcs; uiBoucle++)
	{
		int iPos1; int iPos2;
		ifsFlux >> pcFlux; //D�but 
		pcValeur = GRATraiterMot(pcFlux);
		unsigned int uiTemp = atoi(pcValeur);
		CSommet * somTemp = new CSommet(uiTemp);
		try {
			if (!GRAExistenceSommet(somTemp, &iPos1))
				throw Cexception();
		}
		catch (Cexception exc)
		{
			exc.EXCAfficherException("Erreur lecture : le sommet n'existe pas ");
		}
		delete[] pcValeur;
		delete somTemp;

		//int count = 0;	while (*pcFlux != ',') count++; //Fin
		ifsFlux >> pcFlux;
		pcValeur = GRATraiterMot(pcFlux);
		uiTemp = atoi(pcValeur);
		somTemp = new CSommet(uiTemp);
		try {
			if (!GRAExistenceSommet(somTemp, &iPos2))
				throw Cexception();
		}
		catch (Cexception exc)
		{
			exc.EXCAfficherException("Erreur : le sommet n'existe pas ! ");
		}
		delete[] pcValeur;
		delete somTemp;

		GRAAjouterArcPartant(ppsomGRASommets[iPos1], new CArc(uiTemp));
		GRAAjouterArcArrivant(ppsomGRASommets[iPos2], new CArc(uiTemp));
	}
	ifsFlux >> pcFlux; //]
	delete[] pcFlux;
}

/* ----------------------------------------------------------
Nom : operator <<
-------------------------------------------------------------
Cet operateur permet d'afficher a l'ecran la composition du
graphe
-------------------------------------------------------------
Entree(s)  :  ostream &
			  const CGraphe &
Necessite  :  rien
Sortie(s)  :  ostream &
---------------------------------------------------------- */
ostream & operator<<(ostream & os, const CGraphe & graGrapheParam)
{
	CGraphe graGraphe(graGrapheParam);

	cout << "NBSommets=" << graGraphe.GetGRANbSommets() << endl;
	cout << "NBArcs=" << graGraphe.GetGRANbArcs() << endl;

	cout << "Sommets=[" << endl;
	for (unsigned int uiBoucle = 0; uiBoucle < graGraphe.GetGRANbSommets(); uiBoucle++) {
		cout << "Numero=" << (*(graGraphe.GetGRASommets())[uiBoucle]).GetSOMIdSommet() << endl;
	}
	cout << "]" << endl;

	cout << "Arcs=[" << endl;
	for (unsigned int uiBoucleSommet = 0; uiBoucleSommet < graGraphe.GetGRANbArcs(); uiBoucleSommet++)
	{
		unsigned int NbArc = (*(graGraphe.GetGRASommets())[uiBoucleSommet]).GetSOMNbArcsPartants();
		for (unsigned int uiBoucleArc = 0; uiBoucleArc < NbArc; uiBoucleArc++)
		{
			cout << "Debut=" << (*(graGraphe.GetGRASommets())[uiBoucleSommet]).GetSOMIdSommet() << ", Fin=";
			CArc ** arcTemp = (*(graGraphe.GetGRASommets())[uiBoucleSommet]).GetSOMArcsPartants();
			cout << (*arcTemp[uiBoucleArc]).GetARCDestination() << endl;
		}
	}
	cout << "]" << endl;
	return os;
}
